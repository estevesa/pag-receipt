import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlocksListComponent } from './blocks-list/blocks-list.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { UploadFormDialogComponent } from './upload-form-dialog/upload-form-dialog.component';
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    BlocksListComponent,
    UploadFormDialogComponent,
    HeaderComponent,
    FooterComponent,
  ],
  entryComponents: [UploadFormDialogComponent],
  exports: [
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatTableModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCardModule,
  ],
  providers: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AppModule { }
