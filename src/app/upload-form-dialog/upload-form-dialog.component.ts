import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

interface FormFileData {
  item1: string;
  item2: string;
}
@Component({
  selector: 'app-upload-form-dialog',
  styleUrls: ['./upload-form-dialog.component.scss'],
  templateUrl: './upload-form-dialog.component.html',
})
export class UploadFormDialogComponent implements OnInit {

  public Item1: string;
  public Item2: string;

  public itemFormfields = new FormGroup ({

    item1: new FormControl(''),
    item2: new FormControl(''),

  });

  constructor(
    public dialogRef: MatDialogRef<UploadFormDialogComponent>,
    ) {}

  public saveForm(): void {
    this.Item1 = this.itemFormfields.get('item1').value;
    this.Item2 = this.itemFormfields.get('item2').value;

    console.log(this.Item1, this.Item2);
    console.log(this.itemFormfields);
  }

  public onNoClick(): void {
    this.dialogRef.close();
  }

  public ngOnInit(): void {
  }

}
