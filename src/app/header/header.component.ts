import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UploadFormDialogComponent } from '../upload-form-dialog/upload-form-dialog.component';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  constructor(public dialog: MatDialog) {}
// constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  public ngOnInit(): void {
  }

  public openDialog(): void {
    const dialogRef = this.dialog.open(UploadFormDialogComponent, {
      width: '300px',
    });

    dialogRef.afterClosed().subscribe( result => {
      // this.email = result;
    });
  }

}
