import { Component, Inject, OnInit } from '@angular/core';


import { DocumentList } from './DocumentList';

@Component({
  selector: 'app-blocks-list',
  styleUrls: ['./blocks-list.component.scss'],
  templateUrl: './blocks-list.component.html',
})
export class BlocksListComponent implements OnInit {

  public blockItens = [
    {name: 'Aluguel', description: 'SP', day: '01'},
    {name: 'Cartão', description: 'Itau Card', day: '03'},
    {name: 'Luz', description: 'SP', day: '24'},
    {name: 'Cartão', description: 'Santander', day: '30'},
    {name: 'Consórcio', description: 'Santander', day: '10'},
    {name: 'Cartão', description: 'Nubank', day: '15'},
    {name: 'Celular', description: 'Vivo', day: '15'},
    {name: 'Luz', description: 'Teodoro', day: '13'},
    {name: 'Faculdade', description: 'Cesumar', day: '10'},
    {name: 'E.P', description: 'Santander', day: '30'},
    {name: 'Internet', description: 'Claro', day: '05'},
  ];

  public foods: DocumentList[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'},
  ];

  constructor() {}

  public ngOnInit(): void {
    this.blockItens.sort((a, b) => {
      if ( a.day > b.day ) {
        return 1;
      }
      if ( a.day < b.day ) {
        return -1;
      }
      return 0;
    });
  }

}
